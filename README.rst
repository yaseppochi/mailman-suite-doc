=====================================
Documentation for Mailman Suite Setup
=====================================

This repo is home for the documentation about `Mailman Suite`_.


Building Documentation
======================

In order to build documentation, you need Python 3 and virtualenv installed.

To build, run::

    $ ./build.sh
    # To build and open the documentation in a browser window, you can run:
    $ ./build.sh open


.. _`Mailman Suite`: http://docs.mailman3.org/en/latest/
